﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS18
{
    class Program
    {
        static void Main(string[] args)
        {
            Human human = new Human("Mark", "Goltsberg", "Male", new LivingThing("Marke", "Apartment", new OrganismParameters(14, 152, 186)));

            Human human2 = human.MyClone();

            Console.ReadKey();
        }
    }
}
