﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS18
{
    public class OrganismParameters : IMyCloneable<OrganismParameters>, ICloneable //Параметры, присущие любому живому существу
    {
        int Age, Weight, Height;

        public OrganismParameters(int age, int weight, int height)
        {
            Age = age;
            Weight = weight;
            Height = height;
        }

        public OrganismParameters(OrganismParameters parameters) : this(parameters.Age, parameters.Weight, parameters.Height) 
        {
        }

        public object Clone()
        {
            return MyClone();
        }

        public OrganismParameters MyClone() => new OrganismParameters(Age, Weight, Height);

    }
}
