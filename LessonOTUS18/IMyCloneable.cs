﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS18
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}
