﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS18
{
    public class Human : LivingThing //Человек
    {
        public string FirstName, SecondName, Gender;

        public Human(string firstName, string secondName, string gender, LivingThing livingThing) : base(livingThing)
        {
            FirstName = firstName;
            SecondName = secondName;
            Gender = gender;
        }

        public new Human MyClone()
        {
            LivingThing livingThing = base.MyClone();
            return new Human(FirstName, SecondName, Gender, livingThing);
        }
    }
}
