﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS18
{
    public class LivingThing : OrganismParameters //Живое существо
    {
        string ShortName, LivingPlace;
        OrganismParameters Parameters;

        public LivingThing(string shortName, string livingPlace, OrganismParameters parameters) : base(parameters)
        {
            ShortName = shortName;
            LivingPlace = livingPlace;
            Parameters = parameters;
        }

        public LivingThing(LivingThing livingThing) : this(livingThing.ShortName, livingThing.LivingPlace, livingThing.Parameters)
        {
        }


        public new LivingThing MyClone()
        {
            OrganismParameters organismParameters = base.MyClone();
            return new LivingThing(ShortName, LivingPlace, organismParameters);
        }
    }
}